"use strict";

// Loader

setTimeout(() => {
  $(".loader_bg").fadeToggle();
}, 1000);

// Sticky Navbar

const nav = document.querySelector(".navbar");
const navHeight = nav.getBoundingClientRect().height;
const showcaseArea = document.querySelector(".showcase-area");

const stickyNav = function (entries) {
  const [entry] = entries;

  if (!entry.isIntersecting) nav.classList.add("fixed-top");
  else nav.classList.remove("fixed-top");
};

const navbarObserver = new IntersectionObserver(stickyNav, {
  root: null,
  threshold: 0,
  rootMargin: `-${navHeight}px`,
});
navbarObserver.observe(showcaseArea);

// Revealing Elements

const allSections = document.querySelectorAll("section");
const revealSection = function (entries, observer) {
  const [entry] = entries;

  if (!entry.isIntersecting) return;
  entry.target.classList.remove("section-hidden");
  observer.unobserve(entry.target);
};

const sectionObserver = new IntersectionObserver(revealSection, {
  root: null,
  threshold: 0.15,
});

allSections.forEach(function (section) {
  sectionObserver.observe(section);
  section.classList.add("section-hidden");
});
